const sonarqubeScanner = require("sonarqube-scanner");
sonarqubeScanner(
    {
        serverUrl: "http://localhost:9000",
        token: "YOUR-TOKEN-HERE",
        options: {
            "sonar.sources": "./",
            "sonar.exclusions": "**/__tests__/**, **/*.java",
            "sonar.tests": "./__tests__",
            "sonar.test.inclusions": "./__tests__/**/*.test.tsx,./src/__tests__/**/*.test.ts",
            "sonar.typescript.lcov.reportPaths": "coverage/lcov.info",
            "sonar.testExecutionReportPaths": "reports/test-report.xml",
            "sonar.login": "admin",
            "sonar.password": "admin",
        },
    },
    () => {},
);